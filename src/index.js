/**
 * Welcome to Cloudflare Workers! This is your first worker.
 *
 * - Run `npx wrangler dev src/index.js` in your terminal to start a development server
 * - Open a browser tab at http://localhost:8787/ to see your worker in action
 * - Run `npx wrangler publish src/index.js --name my-worker` to publish your worker
 *
 * Learn more at https://developers.cloudflare.com/workers/
 */

const beers = require("./beers.json")

export default {
	async fetch(request, env, ctx) {
		const url = new URL(request.url);
		const params = url.searchParams;
		const abvMin = parseFloat(params.get('abv_min') || 0);

		const abvMax = parseFloat(params.get('abv_max') || Number.MAX_SAFE_INTEGER);
		const ibuMin = parseFloat(params.get('ibu_min') || 0);
		const ibuMax = parseFloat(params.get('ibu_max') || Number.MAX_SAFE_INTEGER);
		const beerType = params.get('beer_type');
		const  beerTypes = beerType ? beerType.split(',').map(e => e.toLowerCase()) : null


		let filteredBeers = beers.filter(beer => {
			return beer.abv >= abvMin && beer.abv <= abvMax && beer.ibu >= ibuMin && beer.ibu <= ibuMax;
		});

		if (beerType) {
			const beerTypeCompare = beerType.toLowerCase()
			filteredBeers = filteredBeers.filter(beer => beerTypes.includes(beer.type.toLowerCase()))
		}

		return new Response(JSON.stringify(filteredBeers), {
			headers: { 
				'Content-Type': 'application/json',
		        'Access-Control-Allow-Origin': '*',
      			'Access-Control-Allow-Methods': 'GET,HEAD,POST,OPTIONS',
      			'Access-Control-Max-Age': '86400',
			},
		});
	},
};
