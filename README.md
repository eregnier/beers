# Beers API

A simple beers api for education purposes.

and open api yaml spec is provided to understand how to use it (apidoc)[./openapi.yaml], usable with copy paste on https://editor.swagger.io/

API is reachable @ https://beers.utop.workers.dev . 

You can use swagger editor to perform queries and test parameters
